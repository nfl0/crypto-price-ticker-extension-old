# Litecoin-lightweight-ticker

Super lightweight minimalist browser extension that displays the current Bitcoin price in USD.

Bitcoin price pulled from the reliable top notch Coingecko's v3 API.

Firefox - https://addons.mozilla.org/en-US/firefox/addon/btc/

Chromium - http://comingsoon.io/

Opera - https://addons.opera.com/en/extensions/details/just-litecoin-ticker-pro/




![concept](https://raw.githubusercontent.com/nfl0/Litecoin-lightweight-ticker/master/Docs/Screenshot.png)
