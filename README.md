# Bitcoin-lightweight-ticker


<p align="center">
    <img src="https://gitlab.com/nfl0/crypto-price-ticker-extension/-/raw/master/Docs/Demo.gif">
</p>

Super lightweight minimalist browser extension that displays the current price in USD for multiple cryptocurrencies.

Available options

 - ETH
 - BCH
 - BSV
 - BTC
 - LTC
 - Zcash
 - NEO
 - Cardano
 - Monero

Price data pulled from the reliable top notch Coingecko's v3 API (10 requests/second).

Firefox - https://addons.mozilla.org/en-US/firefox/addon/btc/

Chromium - http://comingsoon.io/

Opera - https://addons.opera.com/en/extensions/details/just-bitcoin-ticker-pro/




![concept](https://gitlab.com/nfl0/crypto-price-ticker-extension/-/raw/master/Docs/Screenshot.png)
